export const categories = [
  { name: "Opinión", slug: "opinion" },
  { name: "Editorial", slug: "editorial" },
  { name: "Local", slug: "local" },
  { name: "País", slug: "pais" },
  { name: "Mundo", slug: "mundo" },
  { name: "Entrevistas", slug: "entrevistas" },
  { name: "Deportivo", slug: "deportivo" },
  { name: "Entretenimiento", slug: "entretenimiento" },
];

export const social = [
  { name: "twitter", url: "https://twitter.com/elnacionalbo" },
  { name: "youtube", url: "https://www.youtube.com/@elnacionalbo" },
  { name: "instagram", url: "https://www.instagram.com/alnacionalbo/" },
  { name: "tiktok", url: "https://www.tiktok.com/@elnacionalbo" },
];

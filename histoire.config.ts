import { defineConfig } from "histoire";
import { HstSvelte } from "@histoire/plugin-svelte";

export default defineConfig({
  plugins: [HstSvelte()],
  storyMatch: ["src/stories/**/*.story.svelte"],
  setupFile: "/src/histoire-setup.ts",
});

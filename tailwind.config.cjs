const defaultTheme = require("tailwindcss/defaultTheme");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  theme: {
    fontFamily: {
      sans: ["Space Grotesk", ...defaultTheme.fontFamily.sans],
      display: ["Della Respira", ...defaultTheme.fontFamily.sans],
      serif: ["Libre Baskerville", ...defaultTheme.fontFamily.serif],
      decorative: ["Philosopher", ...defaultTheme.fontFamily.serif],
    },
    fontSize: {
      xs: ["11.79px", "160%"],
      sm: ["13.73px", "155%"],
      base: ["16.00px", "150%"],
      lg: ["18.64px", "145%"],
      xl: ["21.72px", "140%"],
      "2xl": ["25.30px", "135%"],
      "3xl": ["29.47px", "130%"],
      "4xl": ["34.34px", "125%"],
      "5xl": ["40.00px", "120%"],
    },
    container: {
      screens: {
        sm: "640px",
        md: "768px",
        lg: "1024px",
        xl: "1280px",
        // "2xl": "1536px",
      },
      center: true,
      padding: "1rem",
    },
    extend: {
      aspectRatio: {
        "4/3": "4 / 3",
      },
    },
  },
  plugins: [],
};

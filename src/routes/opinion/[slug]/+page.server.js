import { getPostBySlug, formatPosts } from "$lib/api.server";

export async function load({ params }) {
  const { posts } = await getPostBySlug(params.slug);

  return {
    post: formatPosts(posts).shift(),
  };
}

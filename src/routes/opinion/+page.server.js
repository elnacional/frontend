import { getPostsByTagSlug, formatPosts } from "$lib/api.server";

export async function load() {
  const { posts: opinions } = await getPostsByTagSlug("opinion", true, 10);
  const { posts: editorials } = await getPostsByTagSlug("editorial", true, 1);

  return {
    opinion: formatPosts(opinions),
    editorial: formatPosts(editorials).shift(),
  };
}

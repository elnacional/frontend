import { getPostBySlug, formatPosts } from "$lib/api.server";
import { lastCommented } from "$lib/disqus.server";

export async function load({ params }) {
  const { posts } = await getPostBySlug(params.slug);

  return {
    post: formatPosts(posts).shift(),
    lastCommented: lastCommented(),
  };
}

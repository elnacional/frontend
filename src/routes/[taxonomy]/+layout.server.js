import { getTagBySlug, getPostsByTagSlug, formatPosts } from "$lib/api.server";

export async function load({ params }) {
  const { posts } = await getPostsByTagSlug(params.taxonomy, true);
  const { tags } = await getTagBySlug(params.taxonomy, true);

  return {
    posts: formatPosts(posts),
    tag: tags ? tags.shift() : {},
  };
}

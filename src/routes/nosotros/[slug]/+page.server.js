import { getPageBySlug } from "$lib/api.server";

export async function load({ params }) {
  const { pages } = await getPageBySlug(params.slug);

  return {
    page: pages.shift(),
  };
}

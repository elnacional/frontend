import { getAuthors, formatAuthors } from "$lib/api.server";

export async function load() {
  const { authors } = await getAuthors();

  return {
    authors: formatAuthors(authors),
  };
}

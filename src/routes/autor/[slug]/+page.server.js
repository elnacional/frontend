import {
  getAuthorBySlug,
  getPostsByAuthorSlug,
  formatPosts,
} from "$lib/api.server";

export async function load({ params }) {
  const { authors } = await getAuthorBySlug(params.slug);
  const { posts } = await getPostsByAuthorSlug(params.slug);

  return {
    author: authors.shift(),
    posts: formatPosts(posts),
  };
}

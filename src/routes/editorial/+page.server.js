import { redirect } from "@sveltejs/kit";
import { getPostsByTagSlug, formatPosts } from "$lib/api.server";

export const load = async () => {
  const { posts: editorials } = await getPostsByTagSlug("editorial", true, 1);
  const dtrl = formatPosts(editorials).shift();
  throw redirect(301, dtrl.url);
};

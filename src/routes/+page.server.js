import { getLastPosts, formatPosts } from "$lib/api.server";

export async function load({ params }) {
  const { posts } = await getLastPosts();

  return {
    posts: formatPosts(posts),
  };
}

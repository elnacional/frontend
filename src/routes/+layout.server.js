import { getNavigation, getSecondary } from "$lib/api.server";

export async function load() {
  const { navigation } = await getNavigation();
  const { secondary } = await getSecondary();

  return {
    navigation,
    secondary,
  };
}

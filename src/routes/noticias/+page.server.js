import { getPopularTags, formatTags } from "$lib/api.server";

export async function load({ params }) {
  const { tags } = await getPopularTags();

  return {
    tags: formatTags(tags),
  };
}

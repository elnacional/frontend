import { getTagBySlug, getPostsByTagSlug, formatPosts } from "$lib/api.server";

export async function load({ params }) {
  const { posts } = await getPostsByTagSlug(params.tag);
  const { tags } = await getTagBySlug(params.tag);

  return {
    posts: formatPosts(posts),
    tag: tags ? tags.shift() : {},
  };
}

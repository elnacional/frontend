import { error } from "@sveltejs/kit";
import { DISQUS_API_KEY } from "$env/static/private";
import { getPostBySlug, formatPosts } from "$lib/api.server";
import { details } from "$lib/disqus.server";

export async function list(start = 1, limit = 5) {
  try {
    return await fetch(
      `https://disqus.com/api/3.0/posts/list.json?api_key=${DISQUS_API_KEY}&forum=elnacionalbo&start=${daysAgoIsoDate(
        start
      )}&limit=${limit}`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export async function lastCommented() {
  let result = [];
  const comments = await list();

  await Promise.all(
    comments.response.map(async (item) => {
      let thread = await details(item.thread);
      let slug = getSlugFromUrl(thread.response.link);
      const { posts } = await getPostBySlug(slug);
      let post = formatPosts(posts).shift();
      result.push(post);
    })
  );

  return result;
}

function daysAgoIsoDate(daysToSubtract = 1) {
  let today = new Date();
  today.setDate(today.getDate() - daysToSubtract);
  return today.toISOString();
}

function getSlugFromUrl(url) {
  return url.split("/").pop();
}

import { error } from "@sveltejs/kit";
import { DISQUS_API_KEY } from "$env/static/private";

export async function details(thread) {
  try {
    return await fetch(
      `https://disqus.com/api/3.0/threads/details.json?api_key=${DISQUS_API_KEY}&thread=${thread}`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

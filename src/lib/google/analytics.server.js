import { BetaAnalyticsDataClient } from "@google-analytics/data";
import { getPostBySlug, formatPosts } from "$lib/api.server";

const client = new BetaAnalyticsDataClient({
  keyFilename: "./src/lib/google/credentials.json",
});

const propertyId = "379924125";

export async function popular(
  startDate = "7daysAgo",
  endDate = "today",
  limit = 5
) {
  const [response] = await client.runReport({
    property: `properties/${propertyId}`,
    dateRanges: [{ startDate, endDate }],
    dimensions: [{ name: "pagePath" }],
    metrics: [{ name: "screenPageViews" }],
    orderBy: [{ fieldName: "screenPageViews", sortOrder: "DESCENDING" }],
    limit,
  });

  const rows = mapPosts(response.rows);

  const data = cleanArray(rows);

  return getPosts(data);
}

async function getPosts(items) {
  let res = [];

  await Promise.all(
    items.map(async (item) => {
      let url = item.dimension.pop();
      const { posts } = await getPostBySlug(url);
      res.push(formatPosts(posts).shift());
    })
  );

  return res;
}

function mapPosts(posts) {
  return posts.map((item) => ({
    dimension: cleanUrl(item.dimensionValues[0].value),
    metric: item.metricValues[0].value,
  }));
}

function cleanArray(array) {
  return array.filter((i) => i.dimension.length > 1);
}

function cleanUrl(url) {
  return url.split("/").filter((w) => w.length > 0);
}

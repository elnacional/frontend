export function slugBelongsTaxonomy(slug, taxonomy) {
  return taxonomy.some((obj) => obj.slug.includes(slug));
}

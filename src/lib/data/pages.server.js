import { error } from "@sveltejs/kit";
import { GHOST_URL, GHOST_API_KEY } from "$env/static/private";

export async function getPages() {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/pages/?key=${GHOST_API_KEY}`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export async function getPageBySlug(slug) {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/pages/slug/${slug}/?key=${GHOST_API_KEY}`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

import { error } from "@sveltejs/kit";
import { HOME_URL, GHOST_URL, GHOST_API_KEY } from "$env/static/private";

export async function getAuthors() {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/authors/?key=${GHOST_API_KEY}`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export async function getAuthorBySlug(slug) {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/authors/slug/${slug}/?key=${GHOST_API_KEY}`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export function formatAuthors(authors) {
  return authors.map((author) => ({
    ...author,
    url: `${HOME_URL}/autor/${author.slug}`,
  }));
}

import { error } from "@sveltejs/kit";
import { HOME_URL, GHOST_URL, GHOST_API_KEY } from "$env/static/private";

export async function getTagBySlug(slug, internal = false) {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/tags/slug/${
        internal ? "hash-" : ""
      }${slug}?key=${GHOST_API_KEY}`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export async function getPopularTags() {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/tags/?key=${GHOST_API_KEY}&include=count.posts&order=count.posts%20DESC&filter=visibility%3Apublic`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export function formatTags(tags) {
  return tags.map((tag) => ({
    name: tag.name,
    slug: tag.slug,
    url: `${HOME_URL}/noticias/${tag?.slug}`,
    count: tag.count,
  }));
}

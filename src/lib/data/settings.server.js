import { error } from "@sveltejs/kit";
import { HOME_URL, GHOST_URL, GHOST_API_KEY } from "$env/static/private";

export async function getSettings() {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/settings/?key=${GHOST_API_KEY}`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export async function getNavigation() {
  const { settings } = await getSettings();
  return { navigation: mapMenu(settings.navigation) };
}

export async function getSecondary() {
  const { settings } = await getSettings();
  return { secondary: mapMenu(settings.secondary_navigation) };
}

function mapMenu(array) {
  return array.map((list) => ({
    title: list.label,
    url: `${HOME_URL}${list.url}`,
  }));
}

// export async function getTaxonomy() {
//   const content = await allTags();

//   const available = content.tags
//     .filter((x) => x.visibility === "internal")
//     .map((item) => ({
//       ...item,
//       name: item.name.replace("#", ""),
//       slug: item.slug.replace("hash-", ""),
//     }));

//   const internal = categories.filter(
//     (item) => available.findIndex((obj) => obj.slug === item.slug) !== -1
//   );

//   const taxonomy = internal.map((list) => ({
//     title: list.name,
//     url: `${HOME_URL}/${list.slug}`,
//   }));

//   return { taxonomy };
// }

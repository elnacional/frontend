import { error } from "@sveltejs/kit";
import { HOME_URL, GHOST_URL, GHOST_API_KEY } from "$env/static/private";

export async function getLastPosts() {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/posts/?key=${GHOST_API_KEY}&include=authors,tags`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export async function getPostsByTagSlug(slug, internal = false, limit = 15) {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/posts/?key=${GHOST_API_KEY}&include=authors,tags&filter=tag:${
        internal ? "hash-" : ""
      }${slug}&limit=${limit}`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export async function getPostsByAuthorSlug(slug) {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/posts/?key=${GHOST_API_KEY}&include=authors,tags&filter=author:${slug}&limit=5`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export async function getPostBySlug(slug) {
  try {
    return await fetch(
      `${GHOST_URL}/ghost/api/content/posts/slug/${slug}/?key=${GHOST_API_KEY}&include=authors,tags`
    ).then((response) => response.json());
  } catch (e) {
    throw error(e);
  }
}

export function formatPosts(posts) {
  return posts.map((post) => ({
    id: post.id,
    title: post.title,
    slug: post.slug,
    url: `${getInternalTag(post.tags)?.url}/${post.slug}`,
    excerpt: post.excerpt,
    html: post.html,
    feature_image: post.feature_image
      ? post.feature_image
      : `${HOME_URL}/images/default.png`,
    primary_tag: getFeaturedTag(post),
    tags: mappingTags(post.tags),
    primary_author: {
      name: post.primary_author.name,
      slug: post.primary_author.slug,
      url: `${HOME_URL}/autor/${post.primary_author.slug}`,
      profile_image: post.primary_author.profile_image,
    },
    published_at: post.published_at,
  }));
}

function getInternalTag(tags) {
  let tag = {};

  if (tags.length > 0) {
    tag = tags
      .filter((i) => i.name.charAt(0) === "#")
      .map((tag) => ({
        name: tag?.name,
        slug: tag?.slug,
        url: `${HOME_URL}/${tag?.slug}`,
      }))
      .shift();
    mappingInternalTag(tag);
  } else {
    tag = {
      name: null,
      slug: null,
      url: HOME_URL,
    };
  }

  return tag;
}

function getFeaturedTag(post) {
  let featured = post.primary_tag
    ? post.primary_tag
    : post.tags.length > 0
    ? post.tags[0]
    : null;

  let tag = {
    name: featured?.name,
    slug: featured?.slug,
    url: `${HOME_URL}/noticias/${featured?.slug}`,
  };

  if (featured !== null && featured.name.charAt(0) === "#") {
    mappingInternalTag(tag);
  }

  return tag;
}

function mappingTags(tags) {
  return tags
    .filter((i) => i.name.charAt(0) !== "#")
    .map((tag) => ({
      name: tag.name,
      slug: tag.slug,
      url: `${HOME_URL}/noticias/${tag.slug}`,
    }));
}

function mappingInternalTag(tag) {
  tag.name = tag.name.replace("#", "");
  tag.slug = tag.slug.replace("hash-", "");
  tag.url = `${HOME_URL}/${tag.slug}`;
}

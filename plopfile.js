export default function (plop) {
  plop.setGenerator("component", {
    description: "Create a new component",
    prompts: [
      {
        // Name your Component
        type: "input",
        name: "name",
        message: "What is the name of your component?",
      },
    ],
    actions: [
      {
        // Create the component files
        type: "addMany",
        destination: "./src/components/{{properCase name}}",
        base: `.templates/component/definition`,
        templateFiles: `.templates/component/definition/*.hbs`,
      },
      {
        // Export component in component/index.ts
        type: "append",
        path: "./src/components/index.ts",
        templateFile: ".templates/component/index.ts.hbs",
      },
      {
        // Create the story files
        type: "addMany",
        destination: "./src/stories/{{properCase name}}",
        base: `.templates/component/histoire`,
        templateFiles: `.templates/component/histoire/*.hbs`,
      },
    ],
  });
}
